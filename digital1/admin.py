
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from digital1.models import *
# Register your models here.
admin.site.register(Profession)
admin.site.register(Profile)
admin.site.register(Video)
admin.site.register(Interest)
admin.site.register(Area)
admin.site.register(Group)
