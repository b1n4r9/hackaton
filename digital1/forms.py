from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from digital1.models import *


class LoginForm(forms.Form):
    username = forms.CharField(label='Краткое наименование организации:', widget=forms.TextInput(attrs={'class':'form-control', 'type':'text'}) )
    password = forms.CharField(label='Пароль:', widget=forms.TextInput(attrs={'class':'form-control', 'type':'password'}) )


class UserRegistrationForm(forms.ModelForm):
    username = forms.CharField(label='Краткое наименование организации:', widget=forms.TextInput(attrs={'class':'form-control', 'type':'text'}) )
    first_name = forms.CharField(label='Полное наименование организации:', widget=forms.TextInput(attrs={'class':'form-control', 'type':'text'}) )
    email    = forms.EmailField(label='Email', widget=forms.TextInput(attrs={'class':'form-control', 'type':'email'}) )
    password = forms.CharField(label='Пароль', widget=forms.TextInput(attrs={'class':'form-control', 'type':'password'}) )
    password2 = forms.CharField(label='Повторите пароль', widget=forms.TextInput(attrs={'class':'form-control', 'type':'password'}) )

    class Meta:
        model = User
        fields = ('username', 'first_name', 'email')

    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('Passwords don\'t match.')
        return cd['password2']


class TestingForm(forms.Form):
    type_choices = [(i['name'],
                     i['name']) for i in Interest.objects.values('name').distinct()]

    interest_list = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple(attrs={"size":10}),
                                          label=u'',
                                          help_text='<small class="form-text text-muted">Список интересов</small>',
                                          choices=type_choices)


"""
class SignUpForm(UserCreationForm):
    name = forms.CharField()
    discription = forms.TextField()
    city = models.CharField()
    class Meta:
       model = User
       fields = ('name', 'discription', 'city', 'password1', 'password2', )
"""
