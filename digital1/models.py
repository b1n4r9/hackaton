from django.db import models
from django.conf import settings
from digital1.models import *
from django.db.models import Model
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.


class Area(models.Model):
    name = models.CharField(max_length=100)
    discription = models.TextField(max_length=500, blank=True)

    def __str__(self):
        return self.name


class Group(models.Model):
    name = models.CharField(max_length=100)
    discription = models.TextField(max_length=500, blank=True)
    area = models.ForeignKey(Area, on_delete = models.CASCADE)

    def __str__(self):
        return self.name


class Profession(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    discription = models.TextField(max_length=500, blank=True)
    interests = models.CharField(max_length=1000, blank=True) #Список интересов
    group = models.ForeignKey(Group, on_delete = models.CASCADE)

    def __str__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    discription = models.TextField(max_length=500)
    city = models.CharField(max_length=100)
    @receiver(post_save, sender=User)
    def new_user(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)
            instance.profile.save()
    def __str__(self):
        return self.name


class Video(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    discription = models.TextField(max_length=500)
    organization = models.ForeignKey(Profile, related_name='example1', on_delete = models.CASCADE, null=True)
    profission = models.ForeignKey(Profession, related_name='example2', on_delete = models.CASCADE, null=True)
    youTubeHref = models.CharField(max_length = 1000, blank=True, null=True)
    upload = models.FileField(upload_to ='media/videos/% Y/% m/% d/', null=True)


class Interest(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name
