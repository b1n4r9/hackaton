from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$' , views.Index.as_view()),
    url(r'^find_profession/$' , views.Find_Profession.as_view()),
    url(r'^register/$', views.Register, name='Register'),
        # login / logout urls
    url(r'^login/$', views.user_login, name='login'),
    url(r'^testing/$' , views.Testing.as_view()),
    url(r'^company/(.*)/$' , views.Company.as_view()),
    url(r'^upload_video/$' ,  views.UploadVideo.as_view()),
]
