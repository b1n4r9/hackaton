from .index import Index
from .find_profession import Find_Profession
from .register import Register
from .login import user_login
from .my_company import Company
from .testing import Testing
from .upload_video import UploadVideo
