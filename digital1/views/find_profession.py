import os
import json
from django.conf import settings
from django.shortcuts import render, redirect
from django.conf import settings
from django.http import HttpResponse
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from celery.result import AsyncResult
from digital1.models import *
from django.utils.decorators import method_decorator
from digital1.models import *

@method_decorator(csrf_exempt, name='dispatch')
class Find_Profession(generic.TemplateView):
    def post(self, request):
        context = []
        areas_objs = Area.objects.all()

        for area in areas_objs:
            areas = {}
            areas["name"] = area
            areas["groups"] = []
            groups_objs = Group.objects.filter(area = area)
            for group in groups_objs:
                groups = {}
                professions = Profession.objects.filter(group = group)
                groups["name"] = group
                groups["professions"] = professions
                areas["groups"].append(groups)
            context.append(areas)


        professions = []
        for key in request.POST.keys():
            if key == "Регионы":
                continue
            if key == "Организации":
                continue
            professions.append(key)

        company_name = request.POST.get("Организации")
        city = request.POST.get("Регионы")

        print("Professions", professions)
        print("Company", company_name)
        print("City", city)
        
        cities = []
        companies = []
        companies_objs = []
        professions_list = []
        c = []
        c_objs = []
        frames = []

        if len(company_name) != 0:
            comp = Profile.objects.get(name = company_name)
            companies = Video.objects.filter(organization = comp).values("youTubeHref")
            companies_objs = Video.objects.filter(organization = comp)
        else:
            companies = Video.objects.all().values("youTubeHref")
            companies_objs = Video.objects.all()

        if len(city) != 0:
            comp_name = Profile.objects.get(city = city)
            cities = Video.objects.filter(organization = comp_name).values("youTubeHref")
        else:
            cities = Video.objects.all().values("youTubeHref")

        if len(professions) != 0:
            for prof in professions:
                prof_obj = Profession.objects.get(name = prof)
                professions_list.append(Video.objects.filter(profission = prof_obj).values("youTubeHref"))
        else:
            professions_list = Video.objects.all().values("youTubeHref")
            # professions_list = [i for i in professions_list]


        print("Profession", professions_list)
        print("Company_name", companies)
        print("cities", cities)
        
        for num, i in enumerate(companies):
            for j in cities:
                if i == j:
                    c.append(i)
                    c_objs.append(companies_objs[num])
                    break

        for num, i in enumerate(c):
            for j in professions_list:
                if i == j:
                    frames.append(companies_objs[num])
                    break

        return render(request,'digital1/find_profession.html', context = {"context":context, "frames":frames})


    def get(self, request):

        context = []
        areas_objs = Area.objects.all()

        for area in areas_objs:
            areas = {}
            areas["name"] = area
            areas["groups"] = []
            groups_objs = Group.objects.filter(area = area)
            for group in groups_objs:
                groups = {}
                professions = Profession.objects.filter(group = group)
                groups["name"] = group
                groups["professions"] = professions
                areas["groups"].append(groups)
            context.append(areas)
        return render(request,'digital1/find_profession.html', context = {"context":context})
