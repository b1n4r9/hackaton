from django.conf import settings
from django.shortcuts import render, redirect
from django.conf import settings
from django.http import HttpResponse
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from digital1.models import *
from django.contrib.auth.models import User

class Index(generic.TemplateView):
    def post(self, request):
        return render(request,'index.html')


    def get(self, request):
        context = {}
        try:
            sessionid = request.COOKIES['sessionid']
            print("Year!")
            try:
                company_name = Profile.objects.get(user=User.objects.get(username = request.user.username)).name
                print(company_name)
                context = {"sessid":True, "company_name":company_name}
            except:
                context = {"sessid":False}
        except KeyError:
            context = {"sessid":False}
        return render(request,'index.html', context = context)
