import os
import json
from django.conf import settings
from django.shortcuts import render, redirect
from django.conf import settings
from django.http import HttpResponse
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from celery.result import AsyncResult
from django.views.decorators.csrf import csrf_exempt

@method_decorator(csrf_exempt, name='dispatch')

class Company(generic.TemplateView):
    def post(self, request, company_name):
        context = {"change":False, "Upload":False}
        context["company_name"] = company_name
        if request.POST.get("upload") != None:
            return redirect("/digital/upload_video/")
        if request.POST.get("change") != None:
            print("Change!")
            context["change"] = True
        return render(request,'digital/my_company.html', context=context)


    def get(self, request, company_name):
        context = {}
        context["company_name"] = company_name
        try:
            sessionid = request.COOKIES['sessionid']
            print("Year!")
            context["sessid"] = True
        except KeyError:
            context["sessid"] = False
        return render(request, 'digital1/my_company.html', context = context)
