import os
import json
from django.shortcuts import render, redirect
from django.conf import settings
from django.http import HttpResponse
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from digital1.forms import *

@method_decorator(csrf_exempt, name='dispatch')
class Testing(generic.TemplateView):
    def post(self, request):
        testing_form = TestingForm(request.POST)
        context = {}
        if request.POST.get("result") != None and testing_form.is_valid():

            check_test = testing_form.cleaned_data['interest_list']

            summa = 0
            profession_list = {}
            for test in check_test:
                try:
                    context['finded_prof'] = True
                    prof = Profession.objects.filter(interests__contains=test)
                    if prof:
                        for p in prof:
                            if p in profession_list:
                                summa += 1
                                profession_list[p] += 1
                            else:
                                profession_list[p] = 1
                                summa += 1
                except:
                    continue

            profession_proc_temp = {}
            if summa > 0:
                for prof in profession_list.keys():
                    profession_proc_temp[prof] = (profession_list[prof] * 100) // summa
                profession_proc = sorted(profession_proc_temp.items(), key=lambda kv: kv[1], reverse=True)

                context['profession_proc'] = profession_proc

        return render(request,'digital1/testing.html', context=context)


    def get(self, request):
        context = {}
        testing_form = TestingForm(request.GET)
        context['testing_form'] = testing_form
        context['test_list'] = True
        return render(request, 'digital1/testing.html', context = context)
