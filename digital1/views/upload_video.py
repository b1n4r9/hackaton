from django.conf import settings
from django.shortcuts import render, redirect
from django.conf import settings
from django.http import HttpResponse
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from digital1.models import *
from django.contrib.auth.models import User
from digital1.models import *

@method_decorator(csrf_exempt, name='dispatch')
class UploadVideo(generic.TemplateView):
    def post(self, request):
        user = User.objects.get(username=request.user.username)
        print(type(user))
        profile = Profile.objects.get(user=user)
        profession = Profession.objects.get(name=request.POST['profession'])
        video = Video(name=request.POST['name'], youTubeHref=request.POST['href'], discription = request.POST['discription'], organization = profile, profission=profession)
        video.save()
        return render(request,'index.html')


    def get(self, request):
        context = {}
        try:
            sessionid = request.COOKIES['sessionid']
            print("Year!")
            company_name = Profile.objects.get(user=User.objects.get(username = request.user.username)).name
            prof = Profession.objects.all()
            print(prof)
            prof = [i.name for i in prof]
            context = {"User":company_name, "Professions":prof}

        except KeyError:
            context = {}
        return render(request,'upload_html.html', context)
